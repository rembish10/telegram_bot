from aiogram import Bot, Dispatcher, executor, types
import logging
import time
import asyncio
from config import BOT_TOKEN, MSG

# logging.basicConfig(level=logging.INFO)


# loop = asyncio.get_event_loop()                # потік для обробки усіх подій
bot = Bot(BOT_TOKEN)                             # бот, класу Bot
dp = Dispatcher(bot)                             # доставщик

if __name__ == "__main__":
    from handlers import dp, send_to_admin
    # executor.start_polling(dp, on_startup=send_to_admin)                   # старт бота, робить запити start_update
    executor.start_polling(dp, on_startup=send_to_admin)