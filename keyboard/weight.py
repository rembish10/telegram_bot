from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

weight = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="Від народження до 10кг.  Група 0+")
        ],
[
            KeyboardButton(text="9-18кг.  Група 1")
        ],
[
            KeyboardButton(text="15-25кг.  Група 2")
        ],
        [
            KeyboardButton(text="22-36кг.  Група 3")
        ],
        [
            KeyboardButton(text="Назад")
        ],
    ],
    resize_keyboard=True
)