from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton

# menu = ReplyKeyboardMarkup(
#     keyboard=[
#         [
#             KeyboardButton(text="ВІК, ВАГА ТА ЗРІСТ", callback_data="age")
#         ],
# [
#             KeyboardButton(text="ЗРУЧНІСТЬ ДЛЯ ДИТИНИ")
#         ],
# [
#             KeyboardButton(text="ВСТАНОВЛЕННЯ В АВТО")
#         ],
#         [
#             KeyboardButton(text="РОЗПОЧАТИ ПІДБІР АВТОКРІСЛА")
#         ],
#         [
#             KeyboardButton(text="ВІДМІНА")
#         ],
#     ],
#     resize_keyboard=False, row_width=2
# )

# button_age = KeyboardButton("ВІК, ВАГА ТА ЗРІСТ")
# button_convenience = KeyboardButton("ЗРУЧНІСТЬ ДЛЯ ДИТИНИ")
# button_installation = KeyboardButton("ВСТАНОВЛЕННЯ В АВТО")
# button_choice = KeyboardButton("РОЗПОЧАТИ ПІДБІР АВТОКРІСЛА")
# button_cancel = KeyboardButton("ВІДМІНА")
# menu = ReplyKeyboardMarkup(resize_keyboard=True, row_width=2).add(button_age,button_convenience,button_installation,button_choice).add(button_cancel)





menu = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(text="ВІК, ВАГА ТА ЗРІСТ", callback_data="age")
        ],
        [
            InlineKeyboardButton(text="ЗРУЧНІСТЬ ДЛЯ ДИТИНИ", callback_data="convenience")
        ],
        [
            InlineKeyboardButton(text="ВСТАНОВЛЕННЯ В АВТО", callback_data="installation")
        ],
        [
            InlineKeyboardButton(text="РОЗПОЧАТИ ПІДБІР АВТОКРІСЛА", callback_data="choice")
        ],
        [
            InlineKeyboardButton(text="ВІДМІНА", callback_data="cancel")
        ],
    ],
)
