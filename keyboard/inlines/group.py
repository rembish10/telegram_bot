from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from keyboard.inlines.main_callback import main_callback

group_0_choice = InlineKeyboardMarkup(row_width=2,
                                     inline_keyboard=[
                                         [
                                             InlineKeyboardButton(text="Primo", callback_data="Primo")
                                         ],
                                         [
                                             InlineKeyboardButton(text="BABY-SAFE", callback_data="BABY-SAFE")
                                         ],
                                         [
                                             InlineKeyboardButton(text="BABY-SAFE iSENSE", callback_data="BABY-SAFE iSENSE")
                                         ],
                                         [
                                             InlineKeyboardButton(text="BABY-SAFE2 I-SIZE", callback_data="BABY-SAFE2 I-SIZE")
                                         ],
                                         [
                                             InlineKeyboardButton(text="BABY-SAFE3 I-SIZE", callback_data="BABY-SAFE3 I-SIZE")                                       ],
                                         [
                                             InlineKeyboardButton(text="Назад", callback_data="cancel")
                                         ],
                                     ],
                                     )