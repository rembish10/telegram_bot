from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from keyboard.inlines.main_callback import main_callback

main_keyboard = InlineKeyboardMarkup(row_width=2,
                                    inline_keyboard=[
                                        [
                                            InlineKeyboardButton(text="ВІК, ВАГА ТА ЗРІСТ",
                                                                 callback_data=main_callback.new(item_name="age"))
                                        ],
                                        [
                                            InlineKeyboardButton(text="ЗРУЧНІСТЬ ДЛЯ ДИТИНИ",
                                                                 callback_data=main_callback.new(
                                                                     item_name="convenience"))
                                        ],
                                        [
                                            InlineKeyboardButton(text="ВСТАНОВЛЕННЯ В АВТО",
                                                                 callback_data=main_callback.new(
                                                                     item_name="installation"))
                                        ],
                                        [
                                            InlineKeyboardButton(text="РОЗПОЧАТИ ПІДБІР АВТОКРІСЛА",
                                                                 callback_data=main_callback.new(item_name="choice"))
                                        ],
                                        [
                                            InlineKeyboardButton(text="ВІДМІНА", callback_data="cancel")
                                        ],
                                    ],
                                    )

weight_choice = InlineKeyboardMarkup(row_width=2,
                                     inline_keyboard=[
                                         [
                                             InlineKeyboardButton(text="Від народження до 10кг.  Група 0+",
                                                                  callback_data="gr_0")
                                         ],
                                         [
                                             InlineKeyboardButton(text="9-18кг.  Група 1",
                                                                  callback_data="gr_1")
                                         ],
                                         [
                                             InlineKeyboardButton(text="15-25кг.  Група 2",
                                                                  callback_data="gr_2")
                                         ],
                                         [
                                             InlineKeyboardButton(text="22-36кг.  Група 3",
                                                                  callback_data="gr_3")
                                         ],
                                         [
                                             InlineKeyboardButton(text="Назад", callback_data="cancel")
                                         ],
                                     ],
                                     )
