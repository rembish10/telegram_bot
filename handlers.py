import time
import logging
from aiogram import types
from aiogram.types import Message, ReplyKeyboardRemove, CallbackQuery
from aiogram.dispatcher.filters import Command, Text

import keyboard.menu, keyboard.weight
from keyboard.inlines import main_keyboard,weight_choice,main_callback, group_0_choice
from main import bot, dp
# from keyboard import menu, weight
from config import admin_id, MSG
from handlers_info import handlers_dict


async def send_to_admin(dp):
    " Функція відправки повідомлення адміну"
    await bot.send_message(chat_id=admin_id, text="Бот активований")
    await bot.send_message(chat_id=admin_id, text="Натисніть /start")


@dp.message_handler(commands=["start"])
async def start_handler(message: types.Message):
    user_id = message.from_user.id
    user_name = message.from_user.first_name
    user_full_name = message.from_user.full_name
    await bot.send_message(user_id, MSG.format(user_full_name))
    time.sleep(2)
    await bot.send_message(user_id, text="Ви можете підібрати автокрісло за наступними критеріями",
                           reply_markup=main_keyboard)




@dp.callback_query_handler(main_callback.filter(item_name="age"))
# lambda callback_query: True
async def get_age_weight(call: CallbackQuery, callback_data: dict):
    await call.answer(cache_time=60)
    item_name = callback_data.get("item_name")
    await call.message.answer(handlers_dict.get(item_name), reply_markup=weight_choice)


@dp.callback_query_handler(main_callback.filter(item_name="convenience"))
async def get_convenience(call: CallbackQuery, callback_data: dict):
    await call.answer(cache_time=60)
    item_name = callback_data.get("item_name")
    await call.message.answer(handlers_dict.get(item_name), reply_markup=main_keyboard)


@dp.callback_query_handler(main_callback.filter(item_name="installation"))
async def get_installation(call: CallbackQuery, callback_data: dict):
    await call.answer(cache_time=60)
    item_name = callback_data.get("item_name")
    await call.message.answer(handlers_dict.get(item_name), reply_markup=main_keyboard)


@dp.callback_query_handler(main_callback.filter(item_name="choice"))
async def get_choice(call: CallbackQuery, callback_data: dict):
    await call.answer(cache_time=60)
    item_name = callback_data.get("item_name")
    await call.message.answer(handlers_dict.get(item_name), reply_markup=weight_choice)


@dp.callback_query_handler(text="cancel")
async def get_cancel(call: CallbackQuery):
    # await call.answer(cache_time=60)
    await call.answer("Назад", show_alert=True)
    await call.message.edit_reply_markup(reply_markup=None)




@dp.callback_query_handler(text="gr_0")
async def get_choice(call: CallbackQuery, callback_data: dict):
    await call.answer(cache_time=60)
    # item_name = callback_data.get("item_name")
    await call.message.answer(handlers_dict.get("gr_0"), reply_markup=group_0_choice)


# @dp.message_handler(text="Від народження до 10кг.  Група 0+")
# async def get_group_0(message: types.Message):
#     await message.answer(handlers_dict.get("Від народження до 10кг.  Група 0+"),
#                          reply_markup=group_0_choice)


# @dp.message_handler(text="9-18кг.  Група 1")
# async def get_group_1(message: types.Message):
#     await message.answer(handlers_dict.get("9-18кг.  Група 1"),
#                          reply_markup=keyboard.weight)




#
#
# @dp.message_handler(text="ВІК, ВАГА ТА ЗРІСТ")
# async def get_age_weight(message: types.Message):
#     await message.answer(handlers_dict.get("ВІК, ВАГА ТА ЗРІСТ"), reply_markup=keyboard.weight)
#
#
# @dp.message_handler(text="ЗРУЧНІСТЬ ДЛЯ ДИТИНИ")
# async def get_convenience(message: types.Message):
#     await message.answer(handlers_dict.get("ЗРУЧНІСТЬ ДЛЯ ДИТИНИ"), reply_markup=menu)
#
#
# @dp.message_handler(text="ВСТАНОВЛЕННЯ В АВТО")
# async def get_installation(message: types.Message):
#     await message.answer(handlers_dict.get("ВСТАНОВЛЕННЯ В АВТО"), reply_markup=menu)
#
#
# @dp.message_handler(text="РОЗПОЧАТИ ПІДБІР АВТОКРІСЛА")
# async def get_choice(message: types.Message):
#     await message.answer("Виберіть вагу дитини",
#                          reply_markup=keyboard.weight)
#
#
# @dp.message_handler(text="ВІДМІНА")
# async def get_cancel(message: types.Message):
#     await message.answer("Побачимось наступного разу", reply_markup=ReplyKeyboardRemove())
#
# @dp.message_handler(Text)
# async def show_menu(message: types.Message):
#     await message.answer("Критерії вибору автокрісла", reply_markup=menu)


# @dp.message_handler(Text)
# async def say_hello(message: Message):
#     """Відловлює будь-який текст від користувача та повертає основну клавіатуру"""
#     print(message)
#     # greeting = get_greeting()
#     if Text:
#         await message.answer(f'Вітаю, {message.chat.first_name}! \n\n'
#                              'Я з радістю допоможу вам з вибором автокрісла для дитини!\n\n'
#                              'Критерії вибору автокрісла:',
#                              reply_markup=keyboard.menu.menu)


